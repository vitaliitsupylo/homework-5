import React, {useState} from 'react';
import {Toggler, TogglerItem} from './components/Toggler';
import Input from './components/Input';
import Select from './components/Select';
import Button from './components/Button';

import './App.scss';


function App() {
    const [userName, setInputName] = useState('');
    const [email, setInputEmail] = useState('');
    const [type, setActiveToggler] = useState('Type-1');
    const [select, setActiveSelect] = useState('Kyiv');
    const [listResult, addResult] = useState([]);
    const [error, setError] = useState(false);
    const arrValueState = [userName, email, type, select];


    const changeToggler = (name, value) => () => {
        setActiveToggler(value);
        setError(false);
    };

    const changeValue = (name) => (even) => {
        name(even.target.value);
        setError(false);
    };

    const submitForm = (even) => {
        even.preventDefault();
        const check = arrValueState.filter(elem => elem.length === 0).length === 0;
        if (check) {
            addResult([...listResult, arrValueState.join(`; `)]);
            setInputName('');
            setInputEmail('');
            setActiveToggler('Type-1');
            setActiveSelect('Kyiv')
        } else {
            setError(true);
        }
    };


    return (
        <div className="App">
            <form className="form" onSubmit={submitForm}>
                <Input name="UserName"
                       type="text"
                       placeholder="User Name"
                       value={userName}
                       handler={changeValue(setInputName)}
                       contentMaxLength={20}
                />
                <Input name="Email"
                       type="email"
                       placeholder="Email"
                       value={email}
                       handler={changeValue(setInputEmail)}
                       contentMaxLength={30}
                />
                <Select name="City"
                        value={select}
                        options={['Kyiv', 'Lviv', 'Harkiv', 'Odesa']}
                        handler={changeValue(setActiveSelect)}
                />
                <Toggler
                    label={type}
                    value={type}
                    changeStatus={changeToggler}
                    name="type">
                    <TogglerItem value="Type-1"/>
                    <TogglerItem value="Type-2"/>
                    <TogglerItem value="Type-3"/>
                    <TogglerItem value="Type-4"/>
                </Toggler>
                {error && <p className="error-filled">Not all fields are filled</p>}
                <Button type="submit">Enter</Button>
                {(listResult.length > 0) ?
                    <ul>{listResult.map((elem, item) =>
                        <li key={item}>{elem}</li>)}</ul> : null}
            </form>
        </div>
    );
}

export default App;

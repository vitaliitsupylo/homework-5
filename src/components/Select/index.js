import React from "react";
import PropTypes from "prop-types";
import './style.scss';


const Select = ({options, value, handler, name}) => {
    return (
        <label className="select">
            <span className="select__title">{name}</span>
            <select value={value}
                    onChange={handler}
                    className="select__value">
                {options.map((elem, item) => {
                    return <option key={item} value={elem}>{elem}</option>
                })}
            </select>
        </label>
    )
};

Select.propTypes = {
    options: PropTypes.array.isRequired,
    value: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired
};

Select.defaultProps = {
    name: 'Title',
    handler: () => console.log('No handler in select!')

};

export default Select;
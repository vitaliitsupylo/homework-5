import React from 'react';
import './style.scss';

export const Toggler = ({label, children, value, changeStatus, name}) => {
    return (
        <div className="toggler">
            <p className="toggler__label">{label}</p>
            <div className="toggler__container">
                {
                    React.Children.map(children, (childItem) => {
                        if (React.isValidElement(childItem)) {
                            return React.cloneElement(childItem, {
                                parentName: name,
                                handler: changeStatus(name, childItem.props.value),
                                active: childItem.props.value === value
                            });
                        } else {
                            return null;
                        }
                    })
                }
            </div>
        </div>
    );
}

export const TogglerItem = ({name, value, active, handler}) => {
    return (
        <div className={active ? "toggler__item active" : "toggler__item"}
             onClick={handler}>
            {name || value}
        </div>
    );
};

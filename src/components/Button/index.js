import React from "react";
import PropTypes from 'prop-types';
import './style.scss';


function Button({type, children}) {
    return (
        <button className="button" type={type}>
            <span>{children}</span>
        </button>
    )
}

Button.propTypes = {
    children: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['button', 'submit']).isRequired
};

Button.defaultProps = {
    type: 'button',
    children: 'Button'
};

export default Button;
